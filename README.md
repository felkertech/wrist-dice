# Privacy
This app accesses the user's microphone in order to execute voice commands. All commands are processed by the device's speech-to-text system and no personal information is stored.