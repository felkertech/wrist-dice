package com.epsilon.n.weardice;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import permissions.dispatcher.ShowsRationale;

@RuntimePermissions
public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MessageApi.MessageListener,
        DicePicker.OnItemClickCallback,
        SensorEventListener {

    private static final String TAG = "DICEROLL";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private static final int SPEECH_REQUEST_CODE = 0;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 2;
    private boolean syncable = false;
    private boolean history = false;
    private ArrayList<Integer> dieHistory;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    private static final Die[] dice = Die.dice;
    private static final Holder h = new Holder();
    GoogleApiClient mGoogleApiClient;
    TTSEngine ttsEngine;
    SettingsManager sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_p);
        setUpAccelerometer();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager(), new SectionsPagerAdapter.onRollListener() {
            @Override
            public void onRoll(int roll) {
                if(history) {
                    dieHistory.add(roll);
                    Log.d(TAG, "I rolled "+roll);
                    Iterator<Integer> rolls = dieHistory.iterator();
                    TextView tv = (TextView) findViewById(R.id.history_notes);
                    tv.setText("");
                    int sum = 0;
                    while(rolls.hasNext()) {
                        int r = rolls.next();
                        sum += r;
                        tv.setText(tv.getText()+""+r);
                        if(rolls.hasNext()) {
                            tv.setText(tv.getText()+" + ");
                        } else {
                            tv.setText(tv.getText()+" = ");
                        }
                    }
                    ((TextView) findViewById(R.id.history_summation)).setText(sum+"");
                }
            }
        });

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        h.setContext(getApplicationContext());
        h.setDice(dice);
        Log.d(TAG, "About to create TTSEngine");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
            }
        });

        /*ttsEngine = new TTSEngine(getApplicationContext(), findViewById(R.id.SpeechRecognizer), speechRecognizer);
        findViewById(R.id.SpeechRecognizer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
            }
        });*/
//        ttsEngine.createListener();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.d(TAG, "onConnected: " + bundle);
                        // Now you can use the data layer API
                        Log.d(TAG, "Sync up data");
                        setUpSettingsManager();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.d(TAG, "onConnectionSuspended: " + i);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApiIfAvailable(Wearable.API)
                .build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.actionbar);
        setSupportActionBar(toolbar);

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch(id) {
                    case R.id.action_settings:
                        Intent i = new Intent(MainActivity.this, ApplicationSettings.class);
                        startActivity(i);
                        return true;
                    case R.id.action_voice:
                        DialogFragment newFragment = new HelpDialog();
                        newFragment.show(getFragmentManager(), "missiles");
                        return true;
                    /*case R.id.action_about:
                        DialogFragment aboutAppDialogFragment = new AboutAppDialogFragment();
                        aboutAppDialogFragment.show(getFragmentManager(), "missiles");
                        return true;*/
                    case R.id.action_pin:
                        history = !history;
                        if(history) {
                            item.setIcon(R.drawable.ic_radio_button_on_black_24dp);
                            findViewById(R.id.history_view).setVisibility(View.VISIBLE);
                            ((TextView) findViewById(R.id.history_notes)).setText("");
                            ((TextView) findViewById(R.id.history_summation)).setText("");
                            dieHistory = new ArrayList<Integer>();
                        } else {
                            item.setIcon(R.drawable.ic_radio_button_off_black_24dp);
                            findViewById(R.id.history_view).setVisibility(View.INVISIBLE);
                        }
                        return true;
                }
                return true;
            }
        });
        findViewById(R.id.history_view).setVisibility(View.INVISIBLE);

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.random_p);

        sm = new SettingsManager(this);
    }
    TTSEngine.OnStuffSaid speechRecognizer = new TTSEngine.OnStuffSaid() {
        @Override
        public void onStuffSaid(String stuff) {
//                Log.d(TAG, "Got Text "+stuff);

        }
    };
    public void rollCurrentDie() {
        Log.d(TAG, "Shake it off, roll it ");
        Log.d(TAG, mViewPager.getCurrentItem()+"");
        Log.d(TAG, dice[mViewPager.getCurrentItem()].max+"");
        Log.d(TAG, "DiePos"+(dice[mViewPager.getCurrentItem()]));
        int position = dice[0].find(dice[mViewPager.getCurrentItem()].max);
        Log.d(TAG, position+"");
        mViewPager.findViewWithTag("DiePos" + position).performClick();
    }

    public void doSpeech(String stuff) {
        if(stuff.contains("4") || stuff.contains("6") || stuff.contains("8") || stuff.contains("10") || stuff.contains("20") || stuff.contains("12")) {
            int finder = 4;
            if(stuff.contains("6"))
                finder = 6;
            if(stuff.contains("8"))
                finder = 8;
            if(stuff.contains("10"))
                finder = 10;
            if(stuff.contains("12"))
                finder = 12;
            if(stuff.contains("20"))
                finder = 20;
            mViewPager.setCurrentItem(dice[0].find(finder), true);
            mViewPager.findViewWithTag("DiePos"+(dice[0].find(finder))).performClick();
        } else if(stuff.contains("coin")) {
            mViewPager.setCurrentItem(dice[0].find(2), true);
            mViewPager.findViewWithTag("DiePos"+(dice[0].find(2))).performClick();
        } else if(stuff.contains("percentage")) {
            mViewPager.setCurrentItem(dice[0].find(100), true);
            mViewPager.findViewWithTag("DiePos"+(dice[0].find(100))).performClick();
            Handler h = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    mViewPager.setCurrentItem(dice[0].find(10), true);
                    mViewPager.findViewWithTag("DiePos"+(dice[0].find(10))).performClick();
                }
            };
            h.sendEmptyMessageDelayed(0, 1500);
        } else if(stuff.contains("exit") || stuff.contains("quit")) {
            finish();
        }
    }
    public void setUpSettingsManager() {
        sm.setSyncableSettingsManager(mGoogleApiClient, new SettingsManager.Syncable() {
            @Override
            public void onItemEdited(String key, Object value) {
                sm.pushData();
            }

            @Override
            public void onItemReceived(String key, Object value) {

            }
        });
        sm.pushData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.random_p, menu);
        return true;
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        //This will be more for Wear than mobile
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
        Log.d(TAG, "App pausing");
        mSensorManager.unregisterListener(this);
//        ttsEngine.stopListener();
    }
/*    public void syncStuff() {
        if(syncable) {
            Log.d(TAG, "Syncing to Wear");
            SettingsManager settingsManager = new SettingsManager(this);
            PutDataMapRequest dataMap = PutDataMapRequest.create("/prefs");
            dataMap.getDataMap().putString(getString(R.string.WEAR_SHAPE), settingsManager.getString(getString(R.string.WEAR_SHAPE), "Round"));
            dataMap.getDataMap().putBoolean(getString(R.string.WEAR_VIBRATE), settingsManager.getBoolean(getString(R.string.WEAR_VIBRATE)));
            Log.d(TAG, dataMap.getDataMap().keySet().iterator().next());
            PutDataRequest request = dataMap.asPutDataRequest();
            PendingResult<DataApi.DataItemResult> pendingResult = Wearable.DataApi
                    .putDataItem(mGoogleApiClient, request);
        } else{
            Log.d(TAG, "Syncing isn't ready or unavailable. I'm not sure what to do");
        }
    }*/

    @Override
    public void onItemClicked(int position) {
        mViewPager.setCurrentItem(position, true);
    }

    /**
     *      ANDROID-WEAR DATA LAYER
     *
     */
    /**
     *      GPS
     *
     */
    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        Log.d(TAG, "Got Activity Result "+requestCode+", "+resultCode);
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                Log.d(TAG, "Got CONNECTION_FAILURE_RESOLUTION_REQUEST");
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                        Log.d(TAG, "Activity.RESULT_OK");
                        Toast.makeText(getApplicationContext(), "Cannot Connect to Google Play Services", Toast.LENGTH_SHORT);
                        break;
                }
                break;
            case SPEECH_REQUEST_CODE:
                if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
                    List<String> results = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    String spokenText = results.get(0);
//                    speechRecognizer.onStuffSaid(spokenText);
                    doSpeech(spokenText);
                    // Do something with spokenText
                }
                break;
        }
    }
    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    displaySpeechRecognizer();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Voice actions disabled", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }*/
    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason.
            // resultCode holds the error code.
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }

    /**
     *     LOCATION MANAGER (AND SOME MORE GPS STUFF)
     */
     /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        // Display the connection status
        Log.d(TAG, "Connected to Google Play Services");
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Suspended connection alert  -  probably don't do much
    }
    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
    /*
     * If no resolution is available, display a dialog to the
     * user with the error.
     */
             /*showErrorDialog(connectionResult.getErrorCode());*/
            Toast.makeText(getApplicationContext(), connectionResult.getErrorCode()+" RECEIVED FROM GOOGLE PLAY SERVIES", Toast.LENGTH_SHORT);
        }
    }
    // Create an intent that can start the Speech Recognizer activity
    @NeedsPermission(Manifest.permission.RECORD_AUDIO)
    void displaySpeechRecognizer() {
        try {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
// Start the activity, the intent will be populated with the speech text
            startActivityForResult(intent, SPEECH_REQUEST_CODE);
        } catch(ActivityNotFoundException e) {
            Log.e(TAG, "Microphone capabilities not enabled");
            permissionMicrophoneRational();
        }
    }
    @ShowsRationale(Manifest.permission.RECORD_AUDIO)
    void permissionMicrophoneRational() {
        new MaterialDialog.Builder(this)
                .customView(R.layout.permission_microphone, false)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                                REQUEST_CODE_ASK_PERMISSIONS);
                    }
                })
                .show();
    }

    /* ACCELEROMETER MANAGEMENT */
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private float[] gravity;
    private float[] linear_acceleration;
    public void setUpAccelerometer() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gravity = new float[3];
        linear_acceleration = new float[3];
    }
    @Override
    public void onSensorChanged(SensorEvent event){
        // In this example, alpha is calculated as t / (t + dT),
        // where t is the low-pass filter's time-constant and
        // dT is the event delivery rate.

        final float alpha = (float) 0.8;

        // Isolate the force of gravity with the low-pass filter.
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        // Remove the gravity contribution with the high-pass filter.
        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];
        Log.d(TAG, linear_acceleration[0]+", "+linear_acceleration[1]+", "+linear_acceleration[2]);
        if(linear_acceleration[0] > 5 || linear_acceleration[0] < -5) {
            //Roll again
            rollCurrentDie();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}

