package com.epsilon.n.weardice;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.webkit.WebView;
import android.widget.LinearLayout;

/**
 * Created by N on 9/18/2014.
 */
public class HelpDialog extends DialogFragment {
    public static String VERSION = "1.2";
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        String data = "The Voice Bar at the bottom of the screen can be used for active or passive voice commands.<br>" +
                "When you see the 'Listening' text, speak your command. Then it will process and complete that action.<br>" +
                "By default it always listens, but you can tap the bar to speak as well.<br>" +
                "On a wearable, you can use voice actions by tapping the bar.<br><br>" +
                "Say 'Flip a Coin' to initiate a coin flip.<br>" +
                "Say 'Roll a _' to roll a die of a given number.<br>" +
                "Rolling a 'percentage die' will roll two dice.<br>" +
                "You can also 'exit' or 'quit' the app.";
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.about_dialog, null);
        ((WebView) ll.findViewById(R.id.about_dialog_webview)).loadData(data, "text/html", null);

        builder.setView(ll)
                .setTitle("How to Use Voice Commands");
//            builder.set
        // Create the AlertDialog object and return it
        return builder.create();
    }
}