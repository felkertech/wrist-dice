package com.epsilon.n.weardice;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.prefs.MaterialEditTextPreference;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.Arrays;
import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class ApplicationSettings extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    public static final int EDIT_TEXT = 2;
    public static final int EDIT_TEXT_PREF = 21;
    public static final int LIST_PREF = 3;
    GoogleApiClient mGoogleApiClient;
    SettingsManager sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.application_settings);
//        findViewById(R.id.pref_fragment).setBackgroundColor(getResources().getColor(android.R.color.background_light));
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(android.R.id.content, new SettingsFragment())
                    .commit();
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApiIfAvailable(Wearable.API)
                        // Optionally, add additional APIs and scopes if required.
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        sm = new SettingsManager(this);
    }

    public static class SettingsFragment extends PreferenceFragment {
        GoogleApiClient gapi;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            try {
                bindAbout(R.string.pref_key_about);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        public void bindSummary(final int preference_key, final int preference_type) {
            final SettingsManager sm = new SettingsManager(getActivity());
            final String TAG = "weather:AppSettings";
            switch(preference_type) {
                /*case LIST_PREF:
                    final com.jenzz.materialpreference.Preference lp = (com.jenzz.materialpreference.Preference) findPreference(getString(preference_key));
                    lp.setSummary(getResources().getStringArray(R.array.pref_sync_intervals)[sm.getInt(preference_key)]);
                    lp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference preference) {
                            new MaterialDialog.Builder(getActivity())
                                    .title(lp.getTitle())
                                    .items(R.array.pref_sync_intervals)
                                    .alwaysCallSingleChoiceCallback()
                                    .itemsCallbackSingleChoice(sm.getInt(preference_key), new MaterialDialog.ListCallbackSingleChoice() {
                                        @Override
                                        public boolean onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                                            sm.setInt(R.string.sm_sync_interval, i);
                                            lp.setSummary(getResources().getStringArray(R.array.pref_sync_intervals)[sm.getInt(preference_key)]);
                                            return false;
                                        }
                                    })
                                    .show();

                            return false;
                        }
                    });
                    break;*/
            }
        }
        public void enablePreference(int resId, int boolId) {
            final SettingsManager sm = new SettingsManager(getActivity());
            final com.jenzz.materialpreference.Preference lp = (com.jenzz.materialpreference.Preference) findPreference(getString(resId));
            lp.setEnabled(sm.getBoolean(boolId));
        }
        public void bindAbout(int resId) throws PackageManager.NameNotFoundException {
            final com.jenzz.materialpreference.Preference p =
                    (com.jenzz.materialpreference.Preference) findPreference(getString(resId));
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String v = "Version "+pInfo.versionName;
            String b = "Build "+pInfo.versionCode;
            p.setSummary(v+"\n"+b);
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        sm.setSyncableSettingsManager(mGoogleApiClient, null);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    @Override
    public void finish() {
        super.finish();
//        sm.pushData();
    }
}