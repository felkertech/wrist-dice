package com.epsilon.n.weardice;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;
import java.util.ArrayList;

/**
 * A {@link android.support.v13.app.FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    String TAG = "SectionsPageAdapter";
    public ArrayList<Fragment> dieViews = new ArrayList<Fragment>();
    private onRollListener orl;
    public SectionsPagerAdapter(FragmentManager fm, onRollListener orl) {
        super(fm);
        this.orl = orl;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
//        Log.d(TAG, "Creating instance " + position);
        /*if(position < dieViews.size())
            dieViews.set(position, );
        else
            dieViews.add(DieView.newInstance(position));
        Log.d(TAG, position+" "+dieViews.size()+" "+(position < dieViews.size())+" is for set");*/
        Fragment dv = DieView.newInstance(position, new DieView.OnRollListener() {
            @Override
            public void onRoll(int roll) {
                orl.onRoll(roll);
            }
        });
        return dv;
    }

    @Override
    public int getCount() {
        // Show a page for each die.
        return Die.dice.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    public interface onRollListener {
        void onRoll(int roll);
    }
}