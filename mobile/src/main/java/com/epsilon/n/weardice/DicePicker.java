package com.epsilon.n.weardice;

/**
 * Created by N on 9/18/2014.
 */
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.view.ViewPager;

import java.util.*;
import java.util.Iterator;

public class DicePicker extends Fragment {
    private String TAG = "DicePicker";
    private Die[] dice = Die.dice;
    private ListView lv;
    private OnItemClickCallback mCallback;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        lv = (ListView) inflater.inflate(R.layout.diepicker, container, false);
        if(lv != null) {
            final ArrayList<String> dicelist = new ArrayList<String>();
            Iterator<Die> dieIterator = new ArrayList<Die>(Arrays.asList(dice)).iterator();
            while (dieIterator.hasNext()) {
                Die n = dieIterator.next();
                dicelist.add(String.format(getString(n.getTitle(), n.max)));
            }
            lv.setAdapter(new DieArrayAdapter(getActivity().getApplicationContext(), dicelist, (ViewPager) getActivity().findViewById(R.id.pager)));
        }
        return lv;
    }
    // Container Activity must implement this interface
    public interface OnItemClickCallback {
        public void onItemClicked(int position);
    }

    @Override
    public void onAttach(android.app.Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnItemClickCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnItemClickCallback");
        }
    }


    /**
     * LIST-ADAPTER
     */
    public class DieArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final ArrayList<String> values;
        private final ViewPager mViewPager;

        public DieArrayAdapter(Context context, ArrayList<String> values, ViewPager mViewPager) {
            super(context, R.layout.diepicker, values);
            this.context = context;
            this.values = values;
            this.mViewPager = mViewPager;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.diepicker_item, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.label);
            textView.setText(values.get(position));
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "Click to position " + position);
                    mCallback.onItemClicked(position);
                }
            });
            return rowView;
        }
    }
}