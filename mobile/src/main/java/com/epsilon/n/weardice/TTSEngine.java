package com.epsilon.n.weardice;

/**
 * Created by N on 10/1/2014.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;

public class TTSEngine
{
    TextView mListener;
    String TAG = "TTSENGINE";
    Context context;
    private Intent mSpeechIntent;
    private Bundle speechResults;
    private SpeechRecognizer mSpeechRecognizer;
    private OnStuffSaid mCallback;
    private boolean active;
    public TTSEngine(Context context, View listener, OnStuffSaid callback) {
        this.context = context;
        mListener = (TextView) listener;
        mListener.setText("...");
        Log.d(TAG, "TTSEngine Created");
        mCallback = callback;
    }
    /**
     * Sets the friendly value of the listener
     * @param sts The status to be displayed
     * @return Same string for chaining
     */
    public String setListenerStatus(String sts) {
        mListener.setText(sts);
        return sts;
    }

    /**
     * Sets the background for the listener UI
     * @param c Color resource to use
     * @return The same color for chaining
     */
    public int setListenerColor(int c) {
        mListener.setBackgroundColor(context.getResources().getColor(c));
        return context.getResources().getColor(c);
    }


    public void createListener() {
        Log.d(TAG, "Listener creating");
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        Recognizer mRecognitionListener = new Recognizer();
        mSpeechRecognizer.setRecognitionListener( mRecognitionListener );
        mSpeechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        //mSpeechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");				// i18n
        mSpeechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        mSpeechIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 100);				// To loop every X results
//        startListener();
    }
    public void startListener() {
        mSpeechRecognizer.startListening( mSpeechIntent );
        active = true;
    }
    public void destroyListener() {
        mSpeechRecognizer.cancel();
//        mSpeechRecognizer.stopListening();
        mSpeechRecognizer.destroy();
        active = false;
    }
    public void stopListener() {
        mSpeechRecognizer.cancel();
        mSpeechRecognizer.stopListening();
        active = false;
    }
    public void restartListener() {
        mSpeechRecognizer.cancel();
//        mSpeechRecognizer.stopListening();
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                startListener();
            }
        };
        // sleeper time
        handler.sendEmptyMessageDelayed(0,200);
    }
    public void replaceListener() {
        mSpeechRecognizer.cancel();
//        mSpeechRecognizer.stopListening();
        Handler handlerP = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                destroyListener();
                Handler handlerF = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        createListener();
                        Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                if(active)
                                    startListener();
                                Log.d(TAG, "Replaced Listener");
                            }
                        };
                        // sleeper time
                        handler.sendEmptyMessageDelayed(0, 150);
                    }
                };
                // sleeper time
                handlerF.sendEmptyMessageDelayed(0,150);
            }
        };
        // sleeper time
        handlerP.sendEmptyMessageDelayed(0,150);


    }
    // Container Activity must implement this interface
    public interface OnStuffSaid {
        public void onStuffSaid(String stuff);
    }
    public class Recognizer implements RecognitionListener {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onReadyForSpeech(Bundle params) {
//            Log.d(TAG, "onReadyForSpeech");
            setListenerColor(R.color.speech_ready);
            setListenerStatus("Start Speaking");
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onBeginningOfSpeech() {
            Log.d(TAG, "onBeginningOfSpeech");
            setListenerColor(R.color.speech_begin);
            setListenerStatus("Speaking detected");
            speechResults = null;
        /*AudioManager audio = (AudioManager) context.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(Sounds.SELECTED);*/
        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {
            Log.d(TAG, "onBufferReceived");
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onEndOfSpeech() {
//            Log.d(TAG, "onEndOfSpeech");
            setListenerColor(R.color.speech_end);
            setListenerStatus("Processing...");
            /*if(speechResults == null) {
                setListenerStatus("No Speech Detected");
                Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        restartListener();
                    }
                };
                // sleeper time
                handler.sendEmptyMessageDelayed(0,500);
            } else {
                setListenerStatus(mListener.getText() + " " + speechResults.get(SpeechRecognizer.RESULTS_RECOGNITION).toString());
            }*/
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onError(int error) {
            if(active == false)
                return;
            if(error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {
//                Log.d(TAG, "Timeout Error");
                restartListener();
                return;
            }
//            Log.d(TAG, "onSpeechError "+error);
        /*AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(Sounds.ERROR);*/

            setListenerColor(R.color.speech_error);
            setListenerStatus("Error "+error);
            //Something went wrong
            if(error == SpeechRecognizer.ERROR_RECOGNIZER_BUSY) {
                //8
                setListenerStatus("Recognizer Busy");
                replaceListener();
                return;
            } else if (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {
                //OONO - 6
//                setListenerStatus("Speech Timeout");
                restartListener();
                return;
            } else if (error == SpeechRecognizer.ERROR_CLIENT) {
                //5
                setListenerStatus("Client Error");
                replaceListener();
                return;
            } else if(error == SpeechRecognizer.ERROR_AUDIO) {
                //3
                setListenerStatus("Audio Recording Error");
            } else if(error == SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS) {
                //9
                setListenerStatus("Insufficient Permissions");
            } else if(error == SpeechRecognizer.ERROR_NETWORK) {
                //2
                setListenerStatus("Network Issues");
            } else if(error == SpeechRecognizer.ERROR_NO_MATCH) {
                //7
                setListenerStatus("Speech cannot be matched to text");
                replaceListener();
            } else if(error == SpeechRecognizer.ERROR_SERVER) {
                //4
                setListenerStatus("Server Errors");
            } else if(error == SpeechRecognizer.ERROR_NETWORK_TIMEOUT) {
                //1
                setListenerStatus("Network Timeout...");
            }
            replaceListener();
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onResults(Bundle results) {
            Log.d(TAG, "oR "+results.get(SpeechRecognizer.RESULTS_RECOGNITION).toString());
            setListenerColor(R.color.speech_results);
            ArrayList<String> pR = (ArrayList<String>) results.get(SpeechRecognizer.RESULTS_RECOGNITION);
            String listString = pR.get(0);
            setListenerStatus("Completing Action: '"+listString+"'");
            mCallback.onStuffSaid(listString);
        /*AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(Sounds.SUCCESS);*/
            //Resume speech
            restartListener();
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onPartialResults(Bundle partialResults) {
            Log.d(TAG, "oPR "+partialResults.get(SpeechRecognizer.RESULTS_RECOGNITION).toString());
            setListenerColor(R.color.speech_partial);
            ArrayList<String> pR = (ArrayList<String>) partialResults.get(SpeechRecognizer.RESULTS_RECOGNITION);
            String listString = pR.get(0);
            setListenerStatus('"'+listString+'"');
            speechResults = partialResults;
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
            Log.d(TAG, "Recognition Listener onEvent "+eventType+" and "+params.toString());
        }
    }
}
