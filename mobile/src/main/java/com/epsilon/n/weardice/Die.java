package com.epsilon.n.weardice;

/**
 * Created by N on 9/3/2014.
 */
import android.app.Fragment;

import java.util.Random;
import android.util.Log;

public class Die {
    int max = 4;
    Random dieroll = new Random();
    Fragment diefrag;
    String TAG = "Die";
    public static final Die[] dice = {new Die(2), new Die(4), new Die(6), new Die(8), new Die(12), new Die(20), new Die(100), new Die(10), new Die(30), new Die(1000000)};

    public Die(int max) {
        this.max = max;
    }
    public Die(int max, Fragment diefrag) {
        this.max = max;
        this.diefrag = diefrag;
    }
    public String roll() {
        String out;

        if(max == 10 || max == 100) {
            int result = dieroll.nextInt(9);
            out = result+"";
        } else if(max == 2) {
            int result = dieroll.nextInt(2);
            if(result == 0)
            out = "T";
                    else
            out = "H";
        } else {
            int result = dieroll.nextInt(max)+1;
            out = result+"";
        }

        if(max == 100)
            out = out+"0";
        return out;
    }

    public int getTitle() {
        if(max == 100)
            return R.string.percentage_die;
        else if(max == 2)
            return R.string.coin_flip;
        else
            return R.string.die_name;
    }
    public int find(int count) {
        int index = 0;
        for(Die n: dice) {
            Log.d(TAG, n.max+", "+count+", "+index);
            if(n.max == count) {
                return index;
            }
            index++;
        }
        return -1;
    }
}