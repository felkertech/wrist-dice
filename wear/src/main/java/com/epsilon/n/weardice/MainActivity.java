package com.epsilon.n.weardice;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.speech.RecognizerIntent;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.*;
import com.google.android.gms.common.api.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MessageApi.MessageListener,
        DataApi.DataListener, SensorEventListener {
    private static final String TAG = "DICEROLL";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    Die[] dice = Die.dice;
    private static final Holder h = new Holder();

    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int SPEECH_REQUEST_CODE = 0;
    private GoogleApiClient mGoogleApiClient;
    TTSEngine ttsEngine;

    private boolean history;
    private ArrayList<Integer> dieHistory;
    SettingsManager sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_w);
        Log.d(TAG, "App started");
        setUpAccelerometer();
        WatchViewStub layouter = (WatchViewStub) findViewById(R.id.watch_view_stub);
        layouter.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(final WatchViewStub watchViewStub) {
                Log.d(TAG, "inflated layout");
                h.setContext(getApplicationContext());
                h.setDice(dice);

                mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager(), new SectionsPagerAdapter.onRollListener() {
                    @Override
                    public void onRoll(int roll) {
                        if (history) {
                            dieHistory.add(roll);
                            Log.d(TAG, "I rolled " + roll);
                            Iterator<Integer> rolls = dieHistory.iterator();
                            TextView tv = (TextView) findViewById(R.id.history_notes);
                            tv.setText("");
                            int sum = 0;
                            while (rolls.hasNext()) {
                                int r = rolls.next();
                                sum += r;
                                tv.setText(tv.getText() + "" + r);
                                if (rolls.hasNext()) {
                                    tv.setText(tv.getText() + " + ");
                                } else {
                                    tv.setText(tv.getText() + " = ");
                                }
                            }
                            ((TextView) watchViewStub.findViewById(R.id.history_summation)).setText(sum + "");
                        }
                    }
                });

                // Set up the ViewPager with the sections adapter.
                mViewPager = (ViewPager) watchViewStub.findViewById(R.id.pager);
                if (mViewPager != null)
                    mViewPager.setAdapter(mSectionsPagerAdapter);
                else
                    Log.e(TAG, "Um, nothing displays");
                watchViewStub.findViewById(R.id.action_voice).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displaySpeechRecognizer();
                    }
                });
                final ImageButton historyButton = (ImageButton) watchViewStub.findViewById(R.id.action_history);
                historyButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        history = !history;
                        if (history) {
                            historyButton.setImageDrawable(getDrawable(R.drawable.ic_radio_button_on_white_24dp));
                            watchViewStub.findViewById(R.id.history_view).setVisibility(View.VISIBLE);
                            ((TextView) watchViewStub.findViewById(R.id.history_notes)).setText("");
                            ((TextView) watchViewStub.findViewById(R.id.history_summation)).setText("");
                            dieHistory = new ArrayList<Integer>();
                        } else {
                            historyButton.setImageDrawable(getDrawable(R.drawable.ic_radio_button_off_white_24dp));
                            watchViewStub.findViewById(R.id.history_view).setVisibility(View.INVISIBLE);
                        }
                    }
                });
                sm = new SettingsManager(getApplicationContext());
                ((TextView) watchViewStub.findViewById(R.id.history_notes)).setText("");
                ((TextView) watchViewStub.findViewById(R.id.history_summation)).setText("");
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }
    public void rollCurrentDie() {
        Log.d(TAG, "Shake it off, roll it ");
        Log.d(TAG, mViewPager.getCurrentItem()+"");
        Log.d(TAG, dice[mViewPager.getCurrentItem()].max + "");
        Log.d(TAG, "DiePos"+(dice[mViewPager.getCurrentItem()]));
        int position = dice[0].find(dice[mViewPager.getCurrentItem()].max);
        Log.d(TAG, position + "");
        mViewPager.findViewWithTag("DiePos" + position).performClick();
    }
    public void onStuffSaid(String stuff) {
        if(stuff.contains("4") || stuff.contains("6") || stuff.contains("8") || stuff.contains("10") || stuff.contains("20")) {
            int finder = 4;
            if(stuff.contains("6"))
                finder = 6;
            if(stuff.contains("8"))
                finder = 8;
            if(stuff.contains("10"))
                finder = 10;
            if(stuff.contains("20"))
                finder = 20;
            mViewPager.setCurrentItem(dice[0].find(finder), true);
            mViewPager.findViewWithTag("DiePos"+(dice[0].find(finder))).performClick();
        } else if(stuff.contains("coin")) {
            mViewPager.setCurrentItem(dice[0].find(2), true);
            mViewPager.findViewWithTag("DiePos"+(dice[0].find(2))).performClick();
        } else if(stuff.contains("percentage")) {
            mViewPager.setCurrentItem(dice[0].find(100), true);
            mViewPager.findViewWithTag("DiePos"+(dice[0].find(100))).performClick();
            Handler h = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    mViewPager.setCurrentItem(dice[0].find(10), true);
                    mViewPager.findViewWithTag("DiePos"+(dice[0].find(10))).performClick();
                }
            };
            h.sendEmptyMessageDelayed(0, 1500);
        } else if(stuff.contains("exit") || stuff.contains("quit")) {
            finish();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    @Override
    public void onStop() {
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.random_p, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "App pausing");
        mSensorManager.unregisterListener(this);
//        ttsEngine.stopListener();
    }
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d(TAG, "Message Received: "+messageEvent.getData().toString());
    }
    public void onItemClicked(int position) {
        mViewPager.setCurrentItem(position, true);
    }

    /**
     *      ANDROID-WEAR DATA LAYER
     *
     */
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        Log.d(TAG, "Got Activity Result "+requestCode+", "+resultCode);
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                Log.d(TAG, "Got CONNECTION_FAILURE_RESOLUTION_REQUEST");
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                        Log.d(TAG, "Activity.RESULT_OK");
                        Toast.makeText(getApplicationContext(), "Cannot Connect to Google Play Services", Toast.LENGTH_SHORT);
                        break;
                }
                break;
            case SPEECH_REQUEST_CODE:
                if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
                    List<String> results = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    String spokenText = results.get(0);
                    onStuffSaid(spokenText);
                    // Do something with spokenText
                }
        }
    }
    private boolean servicesConnected() {
        // Check that Google Play services is available
        Log.d(TAG, "Services Connected");
        int resultCode =
                com.google.android.gms.common.GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason.
            // resultCode holds the error code.
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = com.google.android.gms.common.GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }

    /**
     *     LOCATION MANAGER (AND SOME MORE GPS STUFF)
     */
     /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        // Display the connection status
//         Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Connected to Google Play Services");
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Suspended connection alert  -  probably don't do much
    }
    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
    /*
     * If no resolution is available, display a dialog to the
     * user with the error.
     */
             /*showErrorDialog(connectionResult.getErrorCode());*/
            Toast.makeText(getApplicationContext(), connectionResult.getErrorCode()+" RECEIVED FROM GOOGLE PLAY SERVIES", Toast.LENGTH_SHORT);
        }
    }
    public void onDataChanged(DataEventBuffer dataEvents) {
        Iterator<DataEvent> eventIterator = dataEvents.iterator();
        Log.d("TAG", "Hey the data changed");
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_DELETED) {
                Log.d("TAG", "DataItem deleted: " + event.getDataItem().getUri());
            } else if (event.getType() == DataEvent.TYPE_CHANGED) {
                Log.d("TAG", "DataItem changed: " + event.getDataItem().getUri());
                DataMap dataMap = DataMap.fromByteArray(event.getDataItem().getData());
                Log.d(TAG, dataMap.keySet().iterator().next());
                SettingsManager sm = new SettingsManager(this);
                sm.setString(getString(R.string.WEAR_SHAPE), dataMap.getString(getString(R.string.WEAR_SHAPE)));
                sm.setBoolean(getString(R.string.WEAR_VIBRATE), dataMap.getBoolean(getString(R.string.WEAR_VIBRATE)));
                Log.d(TAG, sm.getString(getString(R.string.WEAR_SHAPE)));
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(Integer.toString(count));
                    }
                });*/
            }
        }
    }

    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    /* ACCELEROMETER MANAGEMENT */
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private float[] gravity;
    private float[] linear_acceleration;
    public void setUpAccelerometer() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gravity = new float[3];
        linear_acceleration = new float[3];
    }
    @Override
    public void onSensorChanged(SensorEvent event){
        // In this example, alpha is calculated as t / (t + dT),
        // where t is the low-pass filter's time-constant and
        // dT is the event delivery rate.

        final float alpha = (float) 0.8;

        // Isolate the force of gravity with the low-pass filter.
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        // Remove the gravity contribution with the high-pass filter.
        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];
        Log.d(TAG, linear_acceleration[0]+", "+linear_acceleration[1]+", "+linear_acceleration[2]);
        if(linear_acceleration[0] > 5 || linear_acceleration[0] < -5) {
            //Roll again
            rollCurrentDie();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
