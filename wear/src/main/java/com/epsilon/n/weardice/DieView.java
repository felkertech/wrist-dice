package com.epsilon.n.weardice;

/**
 * Created by N on 9/18/2014.
 */

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
//NOTE MUST CHANGE PREFERENCE TAGS
/**
 * A placeholder fragment containing a simple view.
 */
public class DieView extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "die_label";
    private final String TAG = "DieView";
    public OnRollListener listener;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static DieView newInstance(int sectionNumber, OnRollListener orl) {
        DieView fragment = new DieView();
        fragment.listener = orl;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public DieView() {
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final int pos = getArguments().getInt(ARG_SECTION_NUMBER);
        final View rootView = inflater.inflate(R.layout.die_fragment, container, false);
        final SettingsManager preferenceManager = new SettingsManager(getActivity());
        String shape = preferenceManager.getString(getString(R.string.WEAR_SHAPE), "Round");
        Log.d(TAG, "Shape is " + shape + ", API Level is " + Build.VERSION.SDK_INT + " >= " + Build.VERSION_CODES.JELLY_BEAN);
        if(shape.equals("Square") && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Log.d(TAG, "Switch to square dice");
            rootView.findViewById(R.id.die_result).setBackground(getResources().getDrawable(R.drawable.square_corner));
        }
        rootView.setTag("DiePos"+pos);
        Log.d(TAG, "Created "+rootView.getTag());

        ((TextView) rootView.findViewById(R.id.die_position)).setText(String.valueOf(pos));
        ((TextView) rootView.findViewById(R.id.die_label)).setText(String.format(getString(Die.dice[pos].getTitle()), Die.dice[pos].max));
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView roll = ((TextView) rootView.findViewById(R.id.die_result));
                roll.setText(Die.dice[Integer.parseInt(((TextView) rootView.findViewById(R.id.die_position)).getText().toString())].roll());
                roll.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.pop));
                if(!roll.getText().toString().contains("T") && !roll.getText().toString().contains("H"))
                    listener.onRoll(Integer.parseInt(roll.getText().toString()));
                Log.d(TAG, "Tap");
                if(preferenceManager.getBoolean(getString(R.string.WEAR_VIBRATE))) {
                    Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    Log.d(TAG, "Vibrated");
                    if(vibrator.hasVibrator())
                        vibrator.vibrate(200);
                }
            }
        });
        return rootView;
    }

    public interface OnRollListener {
        void onRoll(int roll);
    }
}