package com.epsilon.n.weardice;

import android.content.Context;

/**
 * This will let me have things like final ints and strings by modifying the values, not the actual pointer
 * Created by N on 9/3/2014.
 */
public class Holder {
    private int integer;
    private String text;
    private Context c;
    private Die[] dice;

    public Holder() {

    }
    public Holder setInteger(int integer) {
        this.integer = integer;
        return this;
    }
    public Holder setText(String string) {
        this.text = string;
        return this;
    }
    public Holder setContext(Context c) {
        this.c = c;
        return this;
    }
    public Holder setDice(Die[] dice) {
        this.dice = dice;
        return this;
    }
    public int getInteger() {
        return integer;
    }
    public String getText() {
        return text;
    }
    public Context getContext() {
        return c;
    }

    public Die[] getDice() {
        return dice;
    }
}
