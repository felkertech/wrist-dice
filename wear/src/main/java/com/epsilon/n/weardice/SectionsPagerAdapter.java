package com.epsilon.n.weardice;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * A {@link android.support.v13.app.FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    String TAG = "SectionsPageAdapter";
    public ArrayList<Fragment> dieViews = new ArrayList<Fragment>();
    private onRollListener orl;
    public SectionsPagerAdapter(FragmentManager fm, onRollListener orl) {
        super(fm);
        this.orl = orl;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment dv = DieView.newInstance(position, new DieView.OnRollListener() {
            @Override
            public void onRoll(int roll) {
                orl.onRoll(roll);
            }
        });
        return dv;
    }

    @Override
    public int getCount() {
        // Show a page for each die.
        return Die.dice.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    public interface onRollListener {
        void onRoll(int roll);
    }
}